package com.bonescorp.skaffoldgradle;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {

    @GetMapping("/hello/{name}")
    public String hello(@PathVariable String name) {
        return "Greetings from Spring Boot! Testing Skaffold New Version 1 for " + name +"\n";
    }

}
